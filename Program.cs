﻿using System;
using System.IO;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            //Adapter dla formatów avi i mp4, aby mogły zostać odtworzone jak mp3 i wav;

            AudioPlayer ap = new AudioPlayer();
            bool nextFormat = true;
            Readme();
            while (nextFormat)
            {

                Console.WriteLine("Enter the format: ");
                string format = Console.ReadLine();
                Console.WriteLine("Enter name of the file: ");
                string name = Console.ReadLine();
                ap.Play(format, name);

                string doRepeat;
                Console.WriteLine("Play another format? Y-yes, N-no");
                do
                {
                    doRepeat = Console.ReadLine().ToUpper();
                    if (doRepeat == "N")
                    {
                        nextFormat = false;
                    }
                    else if (doRepeat == "Y")
                    {
                        nextFormat = true;
                    }
                    else Console.WriteLine("Invalid answer ;) Try again");
                } while (doRepeat != "Y" && doRepeat != "N");
            }


            void Readme()
            {
                Console.WriteLine("# Working file formats and their names:  #");
                Console.WriteLine("# format: mp3, file name: MP3example.mp3 #");
                Console.WriteLine("# format: wav, file name: WAVexample.wav #");
                Console.WriteLine("# format: avi, file name: AVIexample.avi #");
                Console.WriteLine("# format: mp4, file name: MP4example.mp4 #");
                Console.WriteLine("");
            }
        }





        public interface IMediaPlayer
        {
            public void Play(string type, string fileName);
        }
        public interface IAdvancedMediaPlayer
        {
            public void MP4Player(string fileName);
            public void AVIPlayer(string fileName);
        }

        public class MP4Play : IAdvancedMediaPlayer
        {

            public void MP4Player(string fileName)
            {
                Console.WriteLine("Playing: " + fileName);
                string workingDirectory = Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
                string myfile = "/c" + projectDirectory + "\\Resources\\" + fileName;
                System.Diagnostics.Process.Start(@"cmd.exe", myfile);
            }
            public void AVIPlayer(string s)
            {
                //nothing
            }
        }
        public class AVIPlay : IAdvancedMediaPlayer
        {
            public void AVIPlayer(string fileName)
            {

                Console.WriteLine("Playing: " + fileName);
                string workingDirectory = Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
                string myfile = "/c" + projectDirectory + "\\Resources\\" + fileName;
                System.Diagnostics.Process.Start(@"cmd.exe", myfile);
            }
            public void MP4Player(string s)
            {
                //nothing
            }
        }

        class AudioPlayer : IMediaPlayer
        {
            IMediaPlayer mediaAdapter;
            public void Play(string type, string fileName)
            {

                string myfile = null;
                string workingDirectory = Environment.CurrentDirectory;
                string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
                if (type == "mp3" || type == "wav")
                {
                    Console.WriteLine("Playing " + type + " file, name: " + fileName);
                    myfile = "/c" + projectDirectory + "\\Resources\\" + fileName;
                }
                else if (type == "avi" || type == "mp4")
                {
                    mediaAdapter = new MediaAdapter(type);
                    mediaAdapter.Play(type, fileName);
                }
                else
                {
                    Console.WriteLine("The " + type + " format is not suported");
                }
                if (myfile != null)
                {
                    System.Diagnostics.Process.Start(@"cmd.exe", myfile);
                }

            }
        }

        class MediaAdapter : IMediaPlayer
        {
            IAdvancedMediaPlayer AdMePlayer;
            public MediaAdapter(string mediatype)
            {
                if (mediatype == "avi")
                {
                    AdMePlayer = new AVIPlay();
                }
                if (mediatype == "mp4")
                {
                    AdMePlayer = new MP4Play();
                }
            }

            public void Play(string type, string name)
            {
                if (type == "avi")
                {
                    AdMePlayer.AVIPlayer(name);
                }
                if (type == "mp4")
                {
                    AdMePlayer.MP4Player(name);
                }
            }

        }


    }
}



